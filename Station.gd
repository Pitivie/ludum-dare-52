extends StaticBody2D

signal docked
signal stockUpdated
signal destroyed
signal upgraded
signal hurt

export var armorByLvl = 2
export var hullByLvl = 50

## Stocks
var stock = []

## Equipment lvl
var turret = 0
var hull = 0
var armor = 0
var shield = 0

## Internal
var PV
var PVMax

## Upgrade cost
## [Iron, Copper, gold]
var turretUpdate = [
	[10, 8, 0], # lvl 1
	[14, 14, 0], # lvl 2
	[20, 20, 8], # lvl 3
]
var hullUpdate = [
	[16, 0, 0], # lvl 1
	[22, 8, 0], # lvl 2
	[30, 12, 4], # lvl 3
]
var armorUpdate = [
	[20, 8, 0], # lvl 1
	[26, 10, 0], # lvl 2
	[34, 12, 4], # lvl 3
]
var shieldUpdate = [
	[8, 16, 0], # lvl 1
	[10, 20, 0], # lvl 2
	[14, 30, 8], # lvl 3
]

func _ready():
	self.connect("hurt", Global, "_on_station_hurt")
	refreshComponents()

func _on_Dock_body_entered(body):
	if body.name == "Module":
		emit_signal("docked")

func unloadOre(cargo):
	for i in Global.Ore:
		stock[Global.Ore[i]] += cargo[Global.Ore[i]]
	emit_signal("stockUpdated", stock)

func upgrade(part):
	var nextLvl = get(part)
	var cost = get(part+"Update")[nextLvl]
	stock[Global.Ore.IRON] -= cost[Global.Ore.IRON]
	stock[Global.Ore.COPPER] -= cost[Global.Ore.COPPER]
	stock[Global.Ore.GOLD] -= cost[Global.Ore.GOLD]
	set(part, nextLvl+1)
	call(part+"Upgrader")
	emit_signal("stockUpdated", stock)
	emit_signal("upgraded", part)
	
func turretUpgrader():
	if turret == 1:
		$Turret.enable()
		$Turret2.enable()
		$Turret3.enable()
		$Turret4.enable()
	$Turret.setLevel(turret)
	$Turret2.setLevel(turret)
	$Turret3.setLevel(turret)
	$Turret4.setLevel(turret)

func hullUpgrader():
	PV += hullByLvl
	PVMax = 50 + hullByLvl * hull
	

func armorUpgrader():
	pass

func shieldUpgrader():
	$Shield.setLevel(shield)

func refreshComponents():
	# Turret
	if turret >= 1:
		$Turret.enable()
		$Turret2.enable()
		$Turret3.enable()
		$Turret4.enable()
	$Turret.setLevel(turret)
	$Turret2.setLevel(turret)
	$Turret3.setLevel(turret)
	$Turret4.setLevel(turret)

	# Hull
	PV = 50 + hullByLvl * hull
	PVMax = 50 + hullByLvl * hull

	# Armor
	
	# Shield
	$Shield.setLevel(shield)

func hurt(damage):
	var armorPts = armorByLvl * armor
	if damage < armorPts:
		return

	PV -= damage - armorPts
	$Hit.play()
	kaboom()
	emit_signal("hurt")
	if PV <=0:
		emit_signal("destroyed")

func kaboom():
	var kabooms = $kabooms.get_children()
	var kaboom = kabooms[randi() % kabooms.size()]
	kaboom.visible = true
	kaboom.play()
