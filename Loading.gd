extends CanvasLayer


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
func _ready():
	transition()

func transition():
	$AnimationPlayer.play("Fade_to_normal")
	$Noir/Logo/Eclat.play("Eclat")
	$Eclat01.play()
	
func _on_Timer_timeout():
	$AnimationPlayer.play("Fade_to_black")
	
	
func _on_Timer2_timeout():
	self.visible = false

func _on_Timer3_timeout():
	$Noir/Logo/Eclat2.play("Eclat")
	$Eclat02.play()
