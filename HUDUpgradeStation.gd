extends PanelContainer

export var title = "Title"
export var lvl = 0

signal open

func _ready():
	$VBoxContainer/title.text = title
	setLevel(0)

# warning-ignore:shadowed_variable
func setLevel(lvl):
	$VBoxContainer/HBoxContainer/lvl1/ColorRect.visible = true
	$VBoxContainer/HBoxContainer/lvl2/ColorRect.visible = true
	$VBoxContainer/HBoxContainer/lvl3/ColorRect.visible = true

	self.lvl = lvl
	if lvl >= 1:
		$VBoxContainer/HBoxContainer/lvl1/ColorRect.visible = false
	if lvl >= 2:
		$VBoxContainer/HBoxContainer/lvl2/ColorRect.visible = false
	if lvl >= 3:
		$VBoxContainer/HBoxContainer/lvl3/ColorRect.visible = false

func _on_HUDUpgradeStation_mouse_entered():
	self_modulate = Color("#dedede")

func _on_HUDUpgradeStation_mouse_exited():
	self_modulate = Color("#ffffff")

func _on_HUDUpgradeStation_gui_input(event):
	if event.is_pressed():
		emit_signal("open")
