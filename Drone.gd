extends KinematicBody2D

export var speed = 50
export var frequence = 50
var damage
var target

signal touched
signal destroyed

func target(targetPosition):
	target = targetPosition
	
func _physics_process(delta):
	var velocity = position.direction_to(target) * speed
	look_at(target)
	if position.distance_to(target) > 5:
		velocity = move_and_slide(velocity)

func _on_Area2D_body_entered(body):
	if body.name == "Station":
		emit_signal("touched")
		queue_free()


func _on_alienArea_area_entered(area):
	if area.name == "laser":
		emit_signal("destroyed")
		queue_free()
