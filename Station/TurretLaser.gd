extends Node2D

var target
var speed = 600
var level = 1

func target(targetPosition):
	target = targetPosition

func _physics_process(delta):
	position.x += speed * delta
	return

func setLevel(newLevel):
	level = newLevel
	$lvl1.visible = false
	$lvl2.visible = false
	$lvl3.visible = false
	if level == 1:
		$lvl1.visible = true
	elif level == 2:
		$lvl2.visible = true
	elif level == 3:
		$lvl3.visible = true
