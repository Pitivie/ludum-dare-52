extends Node2D

var Laser
var target
var shooting = false

var enable = false
var level = 1
var damage

export var cadenceLvl1 = 0.4 
export var cadenceLvl2 = 0.2 
export var cadenceLvl3 = 0.01 

func _ready():
	Laser = preload("res://Station/TurretLaser.tscn")
	setLevel(level)

func setLevel(newLevel):
	level = newLevel
	$lvl1.visible = false
	$lvl2.visible = false
	$lvl3.visible = false
	if level == 1:
		damage = 10
		self.visible = true
		$lvl1.visible = true
		$shotLeft.wait_time = cadenceLvl1
		$shotMiddle.wait_time = cadenceLvl1
		$shotRight.wait_time = cadenceLvl1
	elif level == 2:
		damage = 10
		self.visible = true
		$lvl2.visible = true
		$shotLeft.wait_time = cadenceLvl2
		$shotMiddle.wait_time = cadenceLvl2
		$shotRight.wait_time = cadenceLvl2
	elif level == 3:
		damage = 1
		self.visible = true
		$lvl3.visible = true
		$shotLeft.wait_time = cadenceLvl3
		$shotMiddle.wait_time = cadenceLvl3
		$shotRight.wait_time = cadenceLvl3

func enable():
	enable = true
	self.visible = true

func setTarget(targetPosition):
	look_at(targetPosition)
	target = targetPosition
	shooting = true

func _on_Area2D_area_entered(area):
	if enable == false:
		return

	if area.name == "alienArea":
		setTarget(area.get_parent().position)
		startClock()

func startClock():
	if level == 1:
		$shotLeft.start()
	if level > 1:
		$shotMiddle.start()

func _on_shotLeft_timeout():
	if shooting == false:
		$shotRight.stop()
		$shotLeft.stop()
		$shotMiddle.stop()
		return
	var laser = Laser.instance()
	laser.position = $spawnLeft.position
	laser.target(target)
	laser.setLevel(level)
	add_child(laser)
	$shotRight.start()
	$Cannon.play()

func _on_shotRight_timeout():
	if shooting == false:
		$shotRight.stop()
		$shotLeft.stop()
		$shotMiddle.stop()
		return
	var laser = Laser.instance()
	laser.position = $spawnRight.position
	laser.target(target)
	laser.setLevel(level)
	add_child(laser)
	$shotLeft.start()
	$Cannon.play()

func _on_Area2D_area_exited(area):
	if area.name == "alienArea":
		shooting = false

func _on_shotMiddle_timeout():
	if shooting == false:
		$shotRight.stop()
		$shotLeft.stop()
		$shotMiddle.stop()
		return
	var laser = Laser.instance()
	laser.position = $spawnMiddle.position
	laser.target(target)
	laser.setLevel(level)
	add_child(laser)
	$Cannon.play()
