extends Node2D

var level
var capacity = 0

func _ready():
	setLevel(0)

# Called when the node enters the scene tree for the first time.
func setLevel(newLevel):
	level = newLevel
	self.visible = false
	$lvl1.visible = false
	$lvl2.visible = false
	$lvl3.visible = false
	if level == 1:
		capacity = 50
		self.visible = true
		$lvl1.visible = true
	elif level == 2:
		capacity = 75
		self.visible = true
		$lvl2.visible = true
	elif level == 3:
		capacity = 100
		self.visible = true
		$lvl3.visible = true

func _on_StationShield_area_entered(area):
	if capacity <= 0:
		self.visible = false
		return
	if area.name == "alienArea":
		var enemie = area.get_parent()
		capacity -= enemie.damage
		enemie.queue_free()
	if area.name == "bossLaser":
		var laser = area.get_parent()
		capacity -= laser.damage
		area.get_parent().queue_free()
