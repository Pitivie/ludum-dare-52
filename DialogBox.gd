extends Control

var dialogue = [
	"TRANSMISSION RECEIVED",
	"Alright HarvCrab-232, we finally got to the wreckage of the terminus system.",
	"The star will soon devour the remains of the wreckage so let's get to work!",
	"To start, head to the nearest debris and operate your drill to cut it out with the LEFT CLICK.",
	"We are counting on you HarvCrab-232"
]

var tutorial2 = [
	"Perfect ! Now use the RIGHT CLICK to harvest the remain materials"	
]

var tutorial3 = [
	"Well done HarvCrab-232, return to the station to drop off the ressources"
]

var tutorial4 = [
	"We are too close of the star and the hull has been damaged.",
	"Upgrade the hull of the station to allow us to initiate a drive Jump"
]

var endzone = [
	"Ready to initiate a drive jump, well done HarvCrab-232. We are leaving this sector"
]





var dialogue_index = 0
var finished = false

func _ready():
	load_dialogue()
	
func _physics_process(delta):
	$"Ind".visible = finished
	if Input.is_action_just_pressed("ui_accept"):
		$Idle.visible = false
		$Chat.visible = true
		load_dialogue()

	
func load_dialogue():
	if dialogue_index < dialogue.size():
		finished = false
		$RichTextLabel.bbcode_text = dialogue[dialogue_index]
		$RichTextLabel.percent_visible = 0
		$Tween.interpolate_property(
			$RichTextLabel, "percent_visible",0,1,1,
			Tween.TRANS_LINEAR, Tween.EASE_IN_OUT
		)
		$Tween.start()
	else:
		queue_free()
	dialogue_index += 1

func _on_Tween_tween_completed(object, key):
	finished = true
