extends Node2D

var station = Vector2(0, 0)

func _process(delta):
	look_at(station)

func _on_Area2D_body_entered(body):
	if body.name == "Station":
		self.visible = false

func _on_Area2D_body_exited(body):
	if body.name == "Station":
		self.visible = true
