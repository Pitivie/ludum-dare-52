extends RigidBody2D

signal explosed(type, minQty, maxQty, position)
var bright = true
var hit = 0
var durationMining = 2 # in second

var sprites = [
	preload("res://Asset/Object/asteroide 1.png"),
	preload("res://Asset/Object/asteroide 2.png"),
	preload("res://Asset/Object/asteroide 3.png"),
	preload("res://Asset/Object/asteroide 4.png")
]

var sector = 0
var sectorLoots = [
	[[Global.Ore.IRON, 0, 0],[Global.Ore.COPPER, 0, 0],[Global.Ore.GOLD, 0, 0]], #Sector1
	[[Global.Ore.IRON, 0, 0],[Global.Ore.COPPER, 0, 0],[Global.Ore.GOLD, 0, 0]], #Sector2
	[[Global.Ore.IRON, 1, 3],[Global.Ore.COPPER, 1, 3],[Global.Ore.GOLD, 0, 0]], #Sector3
	[[Global.Ore.IRON, 1, 3],[Global.Ore.COPPER, 1, 3],[Global.Ore.GOLD, 0, 2]], #Sector4
	[[Global.Ore.IRON, 1, 3],[Global.Ore.COPPER, 1, 3],[Global.Ore.GOLD, 0, 2]]  #Sector5
]

func _ready():
	randomize()
	
	randomize_shape()
	
	self.connect("explosed", Global, "_on_Fragment_Exploded")

func randomize_shape():
	var type = randi() % 4
	var rotation = deg2rad(rand_range(0,360))
	
	var texture: StreamTexture = sprites[type]
	$Sprite.texture = texture
	add_polygon(Vector2.ZERO, texture.get_size())
	self.rotation = rotation

func add_polygon(spriteStart: Vector2, spriteStop: Vector2):
	for poly in sprite_to_polygon(spriteStart, spriteStop):
		var collision_polygon = CollisionPolygon2D.new()
		collision_polygon.polygon = poly
		collision_polygon.position -= Vector2(spriteStop.x/2, spriteStop.y/2 + spriteStart.y*2)
		add_child(collision_polygon)
		$Area.add_child(collision_polygon.duplicate())

func sprite_to_polygon(spriteStart: Vector2, spriteStop: Vector2):
	var data = $Sprite.texture.get_data()
	var bitmap = BitMap.new()
	bitmap.create_from_image_alpha(data)
	var polys = bitmap.opaque_to_polygons(Rect2(spriteStart, spriteStop),1)
	return polys

func _on_Mining_timeout():
	hit += $Mining.wait_time
	bright = !bright
	if (bright) : 
		$Sprite.modulate = Color("#ffffff")
	else:
		$Sprite.modulate = Color("#463131")
	if hit >= durationMining:
		for oreInfo in sectorLoots[sector]:
			emit_signal("explosed", oreInfo[0], oreInfo[1], oreInfo[2], position)
		self.queue_free()
		
func StartDrilling():
	$Mining.start()

func StopDrilling():
	$Mining.stop()
