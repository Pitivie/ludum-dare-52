extends Node

var scene = null
var asteroid = preload("res://Asteroid/Asteroid.tscn")

export var minAsteroid = 20
export var maxAsteroid = 40
export var maxDistanceGeneration = 600
export var sector = 1
var minDistanceGeneration = 350

signal explosed
var stackExplo = 0

func _ready():
	randomize()
	scene = get_parent()
	for i in range(randi() % minAsteroid + maxAsteroid):
		var a = asteroid.instance()
		a.sector = sector - 1
		
		a.position.x = rand_range(-maxDistanceGeneration, maxDistanceGeneration)
		while a.position.x in range(-minDistanceGeneration, minDistanceGeneration):
			a.position.x = rand_range(-maxDistanceGeneration, maxDistanceGeneration) 
			
		a.position.y = rand_range(-maxDistanceGeneration, maxDistanceGeneration)
		while a.position.y in range(-minDistanceGeneration, minDistanceGeneration):
			a.position.y = rand_range(-maxDistanceGeneration, maxDistanceGeneration)
		a.connect("explosed", self, "_on_explosed_asteroid")
		add_child(a)

func _on_explosed_asteroid(type, minQty, maxQty, position):
	stackExplo += 1
	if stackExplo == 3:
		stackExplo = 0
		emit_signal("explosed")
