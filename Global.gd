extends Node

enum State {FLYING, DESTROYED, DOCKED, ON_DIALOG}
enum ShieldState {NOT_INSTALL, DAMAGED, CHARGING, FULL}
enum Ore {IRON, COPPER, GOLD}

signal fragmentExploded
signal oreCollected
signal moduleUpdate
signal stationUpdate

func _on_Fragment_Exploded(type, minQty, maxQty, position):
	emit_signal("fragmentExploded",type, minQty, maxQty, position)

func _on_Ore_Collected(type, qty):
	emit_signal("oreCollected", type, qty)
	
func _on_Module_State_Update(health, shield, shieldState):
	emit_signal("moduleUpdate", health, shield, shieldState)

func _on_station_hurt():
	emit_signal("stationUpdate")
