extends Node2D

export var asteroidToExplosed = 15
var asteroidExplosed = 0
signal completed

func _ready():
	$Module.equipTool("Drill", "Left")
	$Module.equipTool("TractorBeam", "Right")
	$Module.initShield()

func _on_AsteroidGenerator_explosed():
	asteroidExplosed += 1
	if asteroidExplosed == asteroidToExplosed:
		$CanvasLayer/Scenario3.initJump()
		emit_signal("completed")
