extends RigidBody2D

var move = true
var Laser
var shooter = 0

var life = 250

export var attackLaser = 8
signal destroyed

func _ready():
	Laser = preload("res://Boss/BossLaser.tscn")
	$life.value = life
	$life.max_value = life

func shoot(firePosition):
	var laser = Laser.instance()
	laser.position = firePosition
	laser.rotation = deg2rad(90)
	laser.damage = attackLaser
	laser.connect("shot", self, "_on_shot_laser")
	add_child(laser)

func nextShooter():
	shooter +=1
	if shooter >= 4:
		shooter = 0

func selectShooterLeft():
	var turret = get_node("turret" + str(shooter+1))
	return turret.position
	
func selectShooterRight():
	var turret = get_node("turret" + str(8-shooter))
	return turret.position

func _physics_process(delta):
	if move == true:
		apply_central_impulse(Vector2(0,740)*delta)
		rotation = 0
		position.x = 0
	else :
		set_linear_velocity(Vector2.ZERO)

func _on_Arrived_timeout():
	move = false
	$Shoot.start()
	$Shoot2.start()

func _on_Shoot_timeout():
	shoot(selectShooterLeft())
	nextShooter()
	$Cannon.play()

func _on_shot_laser():
	var station = get_parent().get_node("Station")
	station.hurt(attackLaser)


func _on_Shoot2_timeout():
	shoot(selectShooterRight())
	nextShooter()
	$Cannon.play()

func _on_alienArea_area_entered(area):
	if area.name == "laser":
		life -= 1
		$life.value = life
		area.get_parent().queue_free()
		if life <= 0:
			emit_signal("destroyed")
			queue_free()
