extends Node2D

var target
var speed = 200
var damage
signal shot

func target(targetPosition):
	target = targetPosition

func _physics_process(delta):
	position.y += speed * delta
	return


func _on_bossLaser_body_entered(body):
	if body.name == "Station":
		emit_signal("shot")
		queue_free()
