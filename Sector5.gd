extends Node2D

func _ready():
	$Module.equipTool("Laser", "Left")
	$Module.equipTool("TractorBeam", "Right")
	$Module.initShield()

func _on_Boss_destroyed():
	$CanvasLayer/Scenario5.bossDestroyed()
	get_node("/root/Main")._on_victory()
