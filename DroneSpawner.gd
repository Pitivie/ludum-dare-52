extends Position2D

var Drone
var station

export var speed = 50
export var timer = 1
export var delay = 5
export var damage = 6
signal droneDestroyed

func _ready():
	Drone = preload("res://Drone.tscn")
	station = get_parent().get_node("Station")
	$spawn.wait_time = timer
	$delay.wait_time = delay
	$delay.start()

func _on_spawn_timeout():
	var drone = Drone.instance()
	drone.position = self.position
	drone.speed = speed
	drone.damage = damage
	drone.target(station.position)
	drone.connect("touched", self, "_on_touched_drone")
	drone.connect("destroyed", self, "_on_destroyed_drone")
	get_parent().add_child(drone)

func _on_touched_drone():
	station.hurt(damage)

func _on_destroyed_drone():
	emit_signal("droneDestroyed")

func _on_delay_timeout():
	$spawn.start()
