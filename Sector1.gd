extends Node2D

signal completed

func _ready():
	$Module.equipTool("Drill", "Left")
	$Module.equipTool("Grab", "Right")

func _on_Tutorial_finished():
	emit_signal("completed")
