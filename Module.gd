extends KinematicBody2D


var velocity = Vector2()

## Module
export var velocityMax = 150
export var acceleration = 2
export var deceleration = 1
export var strafAcceleration = 1
export var rotationDegree = 100
export var fragmentDrawBackMaxForce = 200
export var hullMaxPoint: float = 1000
var state = Global.State.FLYING
var hullPoint: float = hullMaxPoint
signal destroyed
signal moduleStateUpdate

## Tools
var grab = preload("res://Tool/Grab.tscn")
var drill = preload("res://Tool/Drill.tscn")
var laser = preload("res://Tool/Laser.tscn")
var tractorBeam = preload("res://Tool/TractorBeam.tscn")

var leftTool = null
var rightTool = null

## Stokage
var cargo = []
signal stockUpdated

## Shield
var shieldState = Global.ShieldState.NOT_INSTALL
var shieldPoint: float = 0
export var shieldMaxPoint = 250
export var shieldChargePoint = 8
export var shieldDamagedRate = 5
export var shieldChargeRate = 1

var hit_sound = [
	preload("res://Music/SFX/MetalHit01.mp3"),
	preload("res://Music/SFX/MetalHit02.mp3"),
	preload("res://Music/SFX/MetalHit03.mp3"),
	preload("res://Music/SFX/MetalHit04.mp3")
]

func _ready():
	randomize()
	for i in Global.Ore:
		cargo.append(0)
	$boost.max_value = velocityMax
	Global.connect("oreCollected", self, "grabOre")
	self.connect("moduleStateUpdate", Global, "_on_Module_State_Update")

func _process(delta):
	emitState()

func _physics_process(delta):
	get_input(delta)
	var collision = move_and_collide(velocity * delta, false)
	
	handleCollision(collision)

func getBoostTier():
	var power = (velocity.length() * 100) / velocityMax
	
	if power < 2:
		$thrusterLeft.visible = false
		$thrusterRight.visible = false
		return
		
	$thrusterLeft.play()
	$thrusterRight.play()
	$thrusterLeft.visible = true
	$thrusterRight.visible = true
	if power < 33:
		$thrusterLeft.animation = "low"
		$thrusterRight.animation = "low"
	elif power < 66:
		$thrusterLeft.animation = "normal"
		$thrusterRight.animation = "normal"
	elif power > 66:
		$thrusterLeft.animation = "high"
		$thrusterRight.animation = "high"

func get_input(delta):
	# Detect up/down/left/right keystate and only move when pressed.
	if (state != Global.State.FLYING):
		velocity = Vector2.ZERO
		leftTool.toogle(false)
		rightTool.toogle(false)
		$boost.value = velocity.length()
		getBoostTier()
		return

	#Tools
	if Input.is_action_just_pressed("ui_left_click") && leftTool != null:
		leftTool.toogle(true)
	elif Input.is_action_just_released("ui_left_click") && leftTool != null:
		leftTool.toogle(false)
	if Input.is_action_just_pressed("ui_right_click") && rightTool != null:
		rightTool.toogle(true)
	elif Input.is_action_just_released("ui_right_click") && rightTool != null:
		rightTool.toogle(false)

	#Rotation
	var aimSpeed = deg2rad(rotationDegree * delta)
	if Input.is_action_pressed("ui_left"):
		self.rotation -= aimSpeed
	elif Input.is_action_pressed("ui_right"):
		self.rotation += aimSpeed
	
	#Propultion
	if Input.is_action_pressed('ui_up'):
		velocity += Vector2(acceleration, 0).rotated(self.rotation)
	elif Input.is_action_pressed('ui_down'):
		velocity -= velocity.normalized() * Vector2(deceleration, deceleration)

	#Straf
	if Input.is_action_pressed("ui_rotate_left"):
		velocity += Vector2(0, -strafAcceleration).rotated(self.rotation)
	elif Input.is_action_pressed("ui_rotate_right"):
		velocity += Vector2(0, strafAcceleration).rotated(self.rotation)

	velocity = velocity.limit_length(velocityMax)
	$boost.value = velocity.length()
	getBoostTier()

func grabOre(oreType, quantity):
	cargo[oreType] += quantity
	emit_signal("stockUpdated", oreType, cargo[oreType])

func clearStock():
	for i in Global.Ore:
		cargo[Global.Ore[i]] = 0
		emit_signal("stockUpdated", Global.Ore[i], 0)

func handleCollision(collision:KinematicCollision2D):
	if (collision == null):
		return
	if collision.collider.get_class() != "RigidBody2D":
		return
	
	var body = collision.collider as RigidBody2D
	var collisionForce = Vector2.ZERO 
	var drawBackForce = 0
	
	if body.is_in_group('containsOre'):
		self.handleCollisionDamage()
		collisionForce += (-collision.normal) * 10 * (velocity.length() / velocityMax)
		drawBackForce = fragmentDrawBackMaxForce * (velocity.length() / velocityMax)
	elif body.is_in_group('Ores'):
		collisionForce += (-collision.normal) * 0.05
	elif body.is_in_group('Boss'):
		self.handleCollisionDamage()
		drawBackForce = fragmentDrawBackMaxForce * (velocity.length() / velocityMax)
	
	body.apply_central_impulse(collisionForce)
	velocity += collision.normal * drawBackForce

func handleCollisionDamage():
	if(velocity.length()/velocityMax*100 <= 30) :
		return
	
	var sound_to_play = hit_sound[randi() % hit_sound.size()]
	$Hit.set_stream(sound_to_play)
	$Hit.play()
	
	var damage = round((hullMaxPoint/2)*(velocity.length() / velocityMax))
	var hullDamage = abs(shieldPoint - damage) if (shieldPoint - damage) < 0 else 0
	if (shieldState != Global.ShieldState.NOT_INSTALL):
		shieldPoint = max(0, shieldPoint - damage)
		shieldState = Global.ShieldState.DAMAGED
		$ShieldTimer.wait_time = shieldDamagedRate
		$ShieldTimer.start()
	hullPoint -= hullDamage
	if hullPoint <= 0:
		state = Global.State.DESTROYED
		emit_signal("destroyed")
		self.hide()

func equipTool(name, handPosition):
	var newTool = (
		drill.instance() if name == "Drill" else
		laser.instance() if name == "Laser" else
		grab.instance() if name == "Grab" else
		tractorBeam.instance() if name == "TractorBeam" else
		null
	)
	var oldTool = (
		self.leftTool if handPosition == "Left" else
		self.rightTool if handPosition == "Right" else
		null
	)
	
	if (oldTool != null):
		self.remove_child(oldTool)
	
	if (newTool != null):
		self.add_child(newTool)
		if (handPosition == "Left"):
			self.leftTool = newTool
			newTool.position.y = -12
		elif (handPosition == "Right"):
			self.rightTool = newTool
			newTool.position.y = 11

func initShield():
	shieldState = Global.ShieldState.CHARGING
	_on_ShieldTimer_timeout()

func emitState():
	emit_signal("moduleStateUpdate", hullPoint/hullMaxPoint*100, shieldPoint/shieldMaxPoint*100, shieldState)

func _on_ShieldTimer_timeout():
	if (shieldState == Global.ShieldState.DAMAGED):
		shieldState = Global.ShieldState.CHARGING
	if (shieldState == Global.ShieldState.CHARGING):
		if (shieldPoint + shieldChargePoint > shieldMaxPoint):
			shieldPoint = shieldMaxPoint
			$ShieldTimer.stop()
			shieldState = Global.ShieldState.FULL
		else:
			shieldPoint += shieldChargePoint
			$ShieldTimer.wait_time = shieldChargeRate
			$ShieldTimer.start()
