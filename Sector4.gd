extends Node2D

export var alienToKill = 100
var alienKilled = 0

signal completed

func _ready():
	$Module.equipTool("Laser", "Left")
	$Module.equipTool("TractorBeam", "Right")
	$Module.initShield()

func _on_AlienSpawner_alienDestroyed():
	alienKilled += 1
	if alienKilled == alienToKill:
		$CanvasLayer/Scenario4.initJump()
		emit_signal("completed")
