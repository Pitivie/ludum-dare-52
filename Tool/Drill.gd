extends Node2D

signal start_drilling
signal stop_drilling

var drilling = false

func toogle(state: bool):
	if (drilling == state): 
		return
	drilling = state
	if (drilling):
		$Animation.play()
		$SoundStart.play()
	else: 
		$SoundStart.stop()
		$SoundLoop.stop()
		$SoundEnd.play()
		$Animation.stop()
		$Animation.frame = 0
	emitState()

func emitState():
	if (drilling):
		emit_signal("start_drilling")
	else:
		emit_signal("stop_drilling")

func _on_Drill_area_entered(area):
	var root = area.get_parent()
	if root.is_in_group('containsOre'):
		connect("start_drilling", root, "StartDrilling")
		connect("stop_drilling", root, "StopDrilling")
		emitState()


func _on_Drill_area_exited(area):
	if area  == null:
		return
	var root = area.get_parent()
	if root.is_in_group('containsOre'):
		emit_signal("stop_drilling")
		disconnect("start_drilling", root, "StartDrilling")
		disconnect("stop_drilling", root, "StopDrilling")


func _on_drill_sound_finished():
	if(drilling):
		$SoundLoop.play()
