extends Node2D

signal start_drilling
signal stop_drilling

var drilling = false

func toogle(state: bool):
	if (drilling == state): 
		return
	drilling = state
	get_node("Laser").visible = state
	if (state):
		get_node("Animation").play("Running")
		$Sound.play()
	else:
		get_node("Animation").play("End")
		$Sound.stop()
	emitState()

func emitState():
	if (drilling):
		emit_signal("start_drilling")
	else:
		emit_signal("stop_drilling")

func _on_Laser_area_entered(area):
	var root = area.get_parent()
	if root.is_in_group('containsOre'):
		connect("start_drilling", root, "StartDrilling")
		connect("stop_drilling", root, "StopDrilling")
		emitState()

func _on_Laser_area_exited(area):
	if area  == null:
		return
	var root = area.get_parent()
	if root.is_in_group('containsOre'):
		emit_signal("stop_drilling")
		disconnect("start_drilling", root, "StartDrilling")
		disconnect("stop_drilling", root, "StopDrilling")
