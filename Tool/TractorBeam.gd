extends Node2D

signal grab

var grabing = false

func toogle(state: bool):
	if (grabing == state): 
		return
	grabing = state
	get_node("TractorBeam").visible = state
	if (state):
		$Animation.play()
	else:
		$Animation.stop()
		$Animation.frame = 13
	emitState()

func emitState():
	if (grabing):
		emit_signal("grab")

func _on_TractorBeam_area_entered(area):
	var root = area.get_parent()
	if root.is_in_group("Ores"):
		connect("grab", root, "grab")
		emitState()


func _on_TractorBeam_area_exited(area):
	if area  == null:
		return
	var root = area.get_parent()
	if root.is_in_group("Ores"):
		disconnect("grab", root, "grab")
