extends TextureRect

signal newGame
signal exit

func _on_NewGame_pressed():
	emit_signal("newGame")

func _on_Credit_pressed():
	$Title.hide()
	$Controls.hide()
	$Menu.hide()
	$Credit.show()
	$volume.hide()
	$soundIcon.hide()

func _on_Back_pressed():
	$Title.show()
	$Controls.show()
	$Menu.show()
	$Credit.hide()
	$volume.show()
	$soundIcon.show()

func _on_HSlider_drag_ended(value_changed):
	AudioServer.set_bus_volume_db(AudioServer.get_bus_index("Master"), linear2db($volume.value)-20)
