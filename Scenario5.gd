extends Control

var scenario = [
	[
		"TRANSMISSION RECEIVED",
		"We were now in the home system of the hostile forces. A huge interception group is coming our way.",
		"Alerte ! Our sensors indicate that their flagship is part of the group!",
		"We have to end it now! We have equipped your ship with a new thruster.",
		"You are our last hope HarvCrab-232 !"
	],
	[
		"The hostile flagship is here! It's over!"
	], 
	[
		"The enemy flagship has been destroyed! Well done HarvCrab-232, we're going home."
	]
]

var dialogue_index = 0
var scenario_index = 0
var finished = false
var module = null

func _ready():
	$Portrait.play("Idle")
	Global.connect("fragmentExploded", self, "_on_fragment_exploded")
	Global.connect("oreCollected", self, "_on_ore_collected")
	$Transmission.play()
	load_dialogue()
func _process(delta):
	$"Ind".visible = finished
	if Input.is_action_just_pressed("ui_accept"):
		$Portrait.play("Chat")
		load_dialogue()

func bossDestroyed():
	self.visible = true

	$Transmission.play()
	dialogue_index = 0
	scenario_index = 2
	load_dialogue()

func load_dialogue():
	if dialogue_index < scenario[scenario_index].size():
		finished = false
		$RichTextLabel.bbcode_text = scenario[scenario_index][dialogue_index]
		$RichTextLabel.percent_visible = 0
		$Tween.interpolate_property(
			$RichTextLabel, "percent_visible",0,1,1,
			Tween.TRANS_LINEAR, Tween.EASE_IN_OUT
		)
		$Tween.start()
	else:
		self.visible = false
	dialogue_index += 1

func _on_Tween_tween_completed(object, key):
	finished = true


func _on_BossArrival_timeout():
	self.visible = true
	
	$Transmission.play()
	dialogue_index = 0
	scenario_index = 1
	load_dialogue()
	
	
