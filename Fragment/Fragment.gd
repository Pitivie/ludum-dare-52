extends RigidBody2D

signal explosed(type, minQty, maxQty, position)
var bright = true
var hit = 0
var durationMining = 2 # in second

var sector = 0
var sectorLoots = [
	[[Global.Ore.IRON, 1, 3],[Global.Ore.COPPER, 0, 0],[Global.Ore.GOLD, 0, 0]], #Sector1
	[[Global.Ore.IRON, 1, 4],[Global.Ore.COPPER, 1, 2],[Global.Ore.GOLD, 0, 0]], #Sector2
	[[Global.Ore.IRON, 1, 3],[Global.Ore.COPPER, 1, 3],[Global.Ore.GOLD, 0, 0]], #Sector3
	[[Global.Ore.IRON, 1, 3],[Global.Ore.COPPER, 1, 2],[Global.Ore.GOLD, 0, 1]], #Sector4
	[[Global.Ore.IRON, 1, 3],[Global.Ore.COPPER, 1, 2],[Global.Ore.GOLD, 0, 1]]  #Sector5
]

func _ready():
	randomize()
	
	randomize_shape()
	
	self.connect("explosed", Global, "_on_Fragment_Exploded")

func randomize_shape():
	var size = randi() % 2
	var type = (randi() % 8) if size == 0 else (randi() % 16)
	if size==0 && type == 3: #Sprite 3 on big size is bugged durring polygone generation
		type = 7
	var rotation = deg2rad(rand_range(0,360))
	
	var spriteStart = Vector2(64*type, 0) if size == 0 else Vector2(32*type, 64)
	var spriteStop = Vector2(64, 64) if size == 0 else Vector2(32, 32)
	$Sprite.region_rect = Rect2(spriteStart, spriteStop)
	add_polygon(spriteStart, spriteStop)
	self.rotation = rotation

func add_polygon(spriteStart: Vector2, spriteStop: Vector2):
	for poly in sprite_to_polygon(spriteStart, spriteStop):
		var collision_polygon = CollisionPolygon2D.new()
		collision_polygon.polygon = poly
		collision_polygon.position -= Vector2(spriteStop.x/2, spriteStop.y/2 + spriteStart.y*2)
		add_child(collision_polygon)
		$Area.add_child(collision_polygon.duplicate())

func sprite_to_polygon(spriteStart: Vector2, spriteStop: Vector2):
	var data = $Sprite.texture.get_data()
	var bitmap = BitMap.new()
	bitmap.create_from_image_alpha(data)
	var polys = bitmap.opaque_to_polygons(Rect2(spriteStart, spriteStop),1)
	return polys

func _on_Mining_timeout():
	hit += $Mining.wait_time
	bright = !bright
	if (bright) : 
		$Sprite.modulate = Color("#ffffff")
	else:
		$Sprite.modulate = Color("#463131")
	if hit >= durationMining:
		for oreInfo in sectorLoots[sector]:
			emit_signal("explosed", oreInfo[0], oreInfo[1], oreInfo[2], position)
		self.queue_free()
		
func StartDrilling():
	$Mining.start()

func StopDrilling():
	$Mining.stop()
