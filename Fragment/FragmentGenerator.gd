extends Node

var scene = null
var fragment = preload("res://Fragment/Fragment.tscn")

export var minFragment = 20
export var maxFragment = 40
export var maxDistanceGeneration = 600
export var sector = 1
var minDistanceGeneration = 350

func _ready():
	randomize()
	scene = get_parent()
	for i in range(randi() % maxFragment + minFragment):
		var f = fragment.instance()
		f.sector = sector - 1
		
		f.position.x = rand_range(-maxDistanceGeneration, maxDistanceGeneration)
		while f.position.x in range(-minDistanceGeneration, minDistanceGeneration):
			f.position.x = rand_range(-maxDistanceGeneration, maxDistanceGeneration) 
			
		f.position.y = rand_range(-maxDistanceGeneration, maxDistanceGeneration)
		while f.position.y in range(-minDistanceGeneration, minDistanceGeneration):
			f.position.y = rand_range(-maxDistanceGeneration, maxDistanceGeneration)
		add_child(f)

