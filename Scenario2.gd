extends Control

var scenario = [
	[
		"TRANSMISSION RECEIVED",
		"We managed to get out of the star's gravitational field in time! But our emergency jump must have attracted ",
		"the attention of the drones of the empire, we must hurry to collect the minerals from the surrounding asteroids.",
		"And build turret on the station to protect us from hostile drones.",
		"We have equipped your ship with a new collection device. Good luck HarvCrab-232 !"
	],
	[
		"The drones are practically upon us, hurry up!"
	],
	[
		"More hostile drones are coming ! No time to calculate our next destination.",
		"Hurry to collect as many resources as possible while we initiate an emergency jump"
	]
]

var dialogue_index = 0
var scenario_index = 0
var finished = false
var module = null

func _ready():
	$Portrait.play("Idle")
	Global.connect("fragmentExploded", self, "_on_fragment_exploded")
	Global.connect("oreCollected", self, "_on_ore_collected")
	$Transmission.play()
	load_dialogue()
	
	
func _process(delta):
	$"Ind".visible = finished
	if Input.is_action_just_pressed("ui_accept"):
		$Portrait.play("Chat")
		load_dialogue()


func initJump():
	self.visible = true

	$Transmission.play()
	dialogue_index = 0
	scenario_index = 2
	load_dialogue()

func load_dialogue():
	if dialogue_index < scenario[scenario_index].size():
		finished = false
		$RichTextLabel.bbcode_text = scenario[scenario_index][dialogue_index]
		$RichTextLabel.percent_visible = 0
		$Tween.interpolate_property(
			$RichTextLabel, "percent_visible",0,1,1,
			Tween.TRANS_LINEAR, Tween.EASE_IN_OUT
		)
		$Tween.start()
	else:
		self.visible = false
	dialogue_index += 1

func _on_Tween_tween_completed(object, key):
	finished = true


func _on_DroneArrival_timeout():
	self.visible = true

	$Transmission.play()
	dialogue_index = 0
	scenario_index = 1
	load_dialogue()
