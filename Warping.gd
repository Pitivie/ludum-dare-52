extends Node2D

var finishIn = 6

func _ready():
	$Sprite/thrusterLeft.play()
	$Sprite/thrusterRight.play()

func _process(delta):
	$Sprite.position.x += 10

func _on_finishWarp_timeout():
	$AnimationPlayer.current_animation = "fade"

func _on_remove_timeout():
	queue_free()
