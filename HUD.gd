extends CanvasLayer

var station

func loadHUD():
	station = get_parent().get_tree().get_nodes_in_group("sector")[0].get_node("Station")
	updateStationLevel()
	updateStationState()
	Global.connect("moduleUpdate", self, "updateModuleState")
	Global.connect("stationUpdate", self, "updateStationState")
	$EndGameBox.visible = false

func _process(delta):
	if Input.is_action_just_pressed("ui_cancel"):
		_on_closeStationPanel_pressed()

## Module
func updateModuleStock(type, quantity):
	$ModuleCargo/Container.get_child(type).get_node("Quantity").text = str(quantity)
	
func updateModuleState(health, shield, shieldState):
	$ModuleState/VContainer/HContainer/Health.value = health
	if (shieldState != Global.ShieldState.NOT_INSTALL):
		$ModuleState/VContainer/HContainer2.show()
	$ModuleState/VContainer/HContainer2/Shield.value = shield

## Station
func updateStationStock(stock):
	$Station/VBoxContainer/HBoxContainer/IronLabel.text = str(stock[Global.Ore.IRON])
	$Station/VBoxContainer/HBoxContainer/CopperLabel.text = str(stock[Global.Ore.COPPER])
	$Station/VBoxContainer/HBoxContainer/GoldLabel.text = str(stock[Global.Ore.GOLD])

func updateStationState():
	$StationState/Health.value = station.PV
	$StationState/Health.max_value = station.PVMax

func updateStationLevel():
	get_node("Station/VBoxContainer/turret").setLevel(station.turret)
	get_node("Station/VBoxContainer/hull").setLevel(station.hull)
	get_node("Station/VBoxContainer/armor").setLevel(station.armor)
	get_node("Station/VBoxContainer/shield").setLevel(station.shield)

func updateStationSubPanel(part):
	var currentLvl = station.get(part)
	var panel = part.capitalize()+"Panel"
	if currentLvl == 3:
		get_node(panel+"/VBoxContainer/Control/Label").text = "Level Max!"
		get_node(panel+"/VBoxContainer/Control/HBoxContainer").visible = false
		get_node(panel+"/VBoxContainer/Control/HBoxContainer2").visible = false
		get_node(panel+"/VBoxContainer/Control/HBoxContainer3").visible = false
		get_node(panel+"/VBoxContainer/Button").visible = false
		return
	var nextLvl = station.get(part+"Update")[currentLvl]

	get_node(panel+"/VBoxContainer/HBoxContainer/lvl").text = "LVL " + str(currentLvl)

	get_node(panel+"/VBoxContainer/Control/HBoxContainer").visible = false
	get_node(panel+"/VBoxContainer/Control/HBoxContainer2").visible = false
	get_node(panel+"/VBoxContainer/Control/HBoxContainer3").visible = false
	get_node(panel+"/VBoxContainer/Button").disabled = true

	get_node(panel+"/VBoxContainer/Control/HBoxContainer/Label").modulate = Color("#ffffff")
	get_node(panel+"/VBoxContainer/Control/HBoxContainer2/Label").modulate = Color("#ffffff")
	get_node(panel+"/VBoxContainer/Control/HBoxContainer3/Label").modulate = Color("#ffffff")

	if nextLvl[0] > 0:
		get_node(panel+"/VBoxContainer/Control/HBoxContainer/Label").text = str(station.stock[Global.Ore.IRON])+"/"+str(nextLvl[0])
		get_node(panel+"/VBoxContainer/Control/HBoxContainer").visible = true
		if station.stock[Global.Ore.IRON] < nextLvl[0]:
			get_node(panel+"/VBoxContainer/Control/HBoxContainer/Label").modulate = Color("#ff0000")

	if nextLvl[1] > 0:
		get_node(panel+"/VBoxContainer/Control/HBoxContainer2/Label").text = str(station.stock[Global.Ore.COPPER])+"/"+str(nextLvl[1])
		get_node(panel+"/VBoxContainer/Control/HBoxContainer2").visible = true
		if station.stock[Global.Ore.COPPER] < nextLvl[1]:
			get_node(panel+"/VBoxContainer/Control/HBoxContainer2/Label").modulate = Color("#ff0000")

	if nextLvl[2] > 0:
		get_node(panel+"/VBoxContainer/Control/HBoxContainer3/Label").text = str(station.stock[Global.Ore.GOLD])+"/"+str(nextLvl[2])
		get_node(panel+"/VBoxContainer/Control/HBoxContainer3").visible = true
		if station.stock[Global.Ore.GOLD] < nextLvl[2]:
			get_node(panel+"/VBoxContainer/Control/HBoxContainer3/Label").modulate = Color("#ff0000")
	
	if (station.stock[Global.Ore.IRON] >= nextLvl[0] 
		&& station.stock[Global.Ore.COPPER] >= nextLvl[1] 
		&& station.stock[Global.Ore.GOLD] >= nextLvl[2]):
		get_node(panel+"/VBoxContainer/Button").disabled = false

func openStation():
	$Station.visible = true

func closeAllSubPanel():
	var panels = get_tree().get_nodes_in_group("subPanel")
	for panel in panels:
		panel.visible = false

func _on_turret_open():
	closeAllSubPanel()
	updateStationSubPanel('turret')
	$TurretPanel.visible = true

func _on_hull_open():
	closeAllSubPanel()
	updateStationSubPanel('hull')
	$HullPanel.visible = true

func _on_armor_open():
	closeAllSubPanel()
	updateStationSubPanel('armor')
	$ArmorPanel.visible = true

func _on_shield_open():
	closeAllSubPanel()
	updateStationSubPanel('shield')
	$ShieldPanel.visible = true

func _on_upgrade_turret():
	station.upgrade('turret')
	updateStationSubPanel('turret')
	updateStationLevel()

func _on_upgrade_hull():
	station.upgrade('hull')
	updateStationSubPanel('hull')
	updateStationLevel()

func _on_upgrade_armor():
	station.upgrade('armor')
	updateStationSubPanel('armor')
	updateStationLevel()

func _on_upgrade_shield():
	station.upgrade('shield')
	updateStationSubPanel('shield')
	updateStationLevel()

func _on_closeStationPanel_pressed():
	$Station.visible = false
	closeAllSubPanel()
	var main = get_node("/root/Main")
	main.module.state = Global.State.FLYING

func openEndGameMessage(message):
	$Station.visible = false
	$EndGameBox/VBoxContainer/Message.text = message
	$EndGameBox.visible = true

func _on_MainMenuButton_pressed():
	get_node("/root/Main").displayMenu()
