extends Node2D

var module
var station
var sectorIndex = 0
var Sectors
var WarpAnimation

## Station between sector
var levels = [
	0,0,0,0
]
var stocks = [
	0,0,0
]

var life = 50

func _ready():
	Sectors = [
		preload("res://Sector1.tscn"),
		preload("res://Sector2.tscn"),
		preload("res://Sector3.tscn"),
		preload("res://Sector4.tscn"),
		preload("res://Sector5.tscn"),
	]
	WarpAnimation = preload("res://Warping.tscn")

func _on_Menu_Tutorial():
	var Tutorial = preload("res://Tutorial.tscn")
	add_child(Tutorial.instance())

func _on_module_stockUpdated(type: int, quantity: int):
	$HUD.updateModuleStock(type, quantity)

func _on_module_destroyed():
	self.displayEndGame("Your ship have been destroyed")

func _on_station_destroyed():
	self.displayEndGame("Oh No! You should add\nbetter protection on station")

func _on_victory():
	module.state = Global.State.DOCKED
	self.displayEndGame("You successfully support the station\nThank you for playing!")

func displayEndGame(message):
	$warpFinished.stop()
	$warpSound.stop()
	$HUD.openEndGameMessage(message)
	

func _on_station_stockUpdated(stock):
	$HUD.updateStationStock(stock)

func _on_station_docked():
	$HUD.openStation()
	station.unloadOre(module.cargo)
	module.state = Global.State.DOCKED
	module.clearStock()

func _on_sector_completed():
	$HUD/warpLoading.max_value = $warpFinished.wait_time
	$HUD/warpLoading.visible = true
	$warpFinished.start()
	$warpSound.play()

func _process(delta):
	$HUD/warpLoading.value = $warpFinished.wait_time - $warpFinished.time_left

func _on_warpFinished_timeout():
	levels = [
		station.turret,
		station.hull,
		station.armor,
		station.shield
	]
	stocks = station.stock
	life = station.PV

	sectorIndex += 1
	var sector = get_tree().get_nodes_in_group("sector")[0]
	sector.queue_free()
	$AudioStreamPlayer.stop()
	$warpSound2.play()
	$HUD.visible = false
	var warpAnimation = WarpAnimation.instance()
	add_child(warpAnimation)
	$loadNextSector.wait_time = warpAnimation.finishIn
	$loadNextSector.start()

func _on_loadNextSector_timeout():
	$HUD.visible = true
	displaySector()

func displaySector():
	$HUD/warpLoading.visible = false
	var sector = Sectors[sectorIndex].instance()
	sector.add_to_group("sector")
	sector.connect("completed", self, "_on_sector_completed")
	add_child(sector)

	module = sector.get_node("Module")
	module.connect("stockUpdated", self, "_on_module_stockUpdated")
	module.connect("destroyed", self, "_on_module_destroyed")
#	Debug
	module.grabOre(Global.Ore.IRON, 0)
	module.grabOre(Global.Ore.COPPER, 0)
	module.grabOre(Global.Ore.GOLD, 0)
	station = sector.get_node("Station")
	station.stock = stocks
	station.turret = levels[0]
	station.hull = levels[1]
	station.armor = levels[2]
	station.shield = levels[3]
	station.PV = life
	station.refreshComponents()
	station.connect("docked", self, "_on_station_docked")
	station.connect("stockUpdated", self, "_on_station_stockUpdated")
	station.connect("destroyed", self, "_on_station_destroyed")
	_on_station_stockUpdated(station.stock)

	$HUD.loadHUD()

func _on_Menu_newGame():
	sectorIndex = 0
	$HUD.visible = true
	$Menu.visible = false
	displaySector()

func displayMenu():
	$HUD.visible = false
	$Menu.visible = true
	var sector = get_tree().get_nodes_in_group("sector")[0]
	sector.queue_free()
	levels = [0,0,0,0]
	stocks = [0,0,0]
	life = 50
