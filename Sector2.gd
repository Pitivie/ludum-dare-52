extends Node2D

export var droneToKill = 30
var droneKilled = 0

signal completed

func _ready():
	$Module.equipTool("Drill", "Left")
	$Module.equipTool("TractorBeam", "Right")

func _on_DroneSpawner_droneDestroyed():
	droneKilled += 1
	if droneKilled == droneToKill:
		$CanvasLayer/Scenario2.initJump()
		emit_signal("completed")


