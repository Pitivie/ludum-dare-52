extends RigidBody2D

signal oreCollected(type, qty)

var type = Global.Ore.IRON

func _ready():
	self.connect("oreCollected", Global, "_on_Ore_Collected")

func grab():
	emit_signal("oreCollected", type, 1)
	queue_free()
