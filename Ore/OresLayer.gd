extends Node2D

var ore = preload("res://Ore/Ore.tscn")

var oreTexture = [
	preload("res://Asset/Object/Fer.png"),
	preload("res://Asset/Object/Cuivre.png"),
	preload("res://Asset/Object/Or.png")
]

var spawnPos = [
	[16,0],
	[0,16],
	[-16,-16],
	[8,-8],
	[0,0]
]

func _ready():
	randomize()
	Global.connect("fragmentExploded", self, "addOre")

func addOre(type, minQty, maxQty, orePosition):
	for i in range(round(rand_range(minQty, maxQty))):
		var newOre = ore.instance() as RigidBody2D
		newOre.type = type
		newOre.get_node("Sprite").texture = oreTexture[type]
		orePosition.x += spawnPos[i][0]
		orePosition.y += spawnPos[i][1]
		newOre.set_position(orePosition)
		var impulse = Vector2(rand_range(-30,30), rand_range(-30,30))
		newOre.apply_central_impulse(impulse)
		newOre.apply_torque_impulse(rand_range(-500,500))
		add_child(newOre)
