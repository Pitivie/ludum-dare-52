extends Control

var scenario = [
	[
		"TRANSMISSION RECEIVED",
		"Alert our jump was longer than expected, we are now in an asteroid field.",
		"We can't calculate the next destination as long as the asteroids are in our path!",
		"In addition, our sensors indicates that many asteroids are heading towards us!.",
		"HarvCrab-232 you must clear a path for us through the field and provide us with the materials necessary to protect the station",
		"We have equipped your ship with a new shield. Good luck HarvCrab-232 !"
	], 
	[
		"Ready to initiate a drive jump, well done HarvCrab-232. We are leaving this sector"
	]
]

var dialogue_index = 0
var scenario_index = 0
var finished = false
var module = null

func _ready():
	$Portrait.play("Idle")
	Global.connect("fragmentExploded", self, "_on_fragment_exploded")
	Global.connect("oreCollected", self, "_on_ore_collected")
	$Transmission.play()
	load_dialogue()
func _process(delta):
	$"Ind".visible = finished
	if Input.is_action_just_pressed("ui_accept"):
		$Portrait.play("Chat")
		load_dialogue()

func initJump():
	self.visible = true

	$Transmission.play()
	dialogue_index = 0
	scenario_index = 1
	load_dialogue()

func load_dialogue():
	if dialogue_index < scenario[scenario_index].size():
		finished = false
		$RichTextLabel.bbcode_text = scenario[scenario_index][dialogue_index]
		$RichTextLabel.percent_visible = 0
		$Tween.interpolate_property(
			$RichTextLabel, "percent_visible",0,1,1,
			Tween.TRANS_LINEAR, Tween.EASE_IN_OUT
		)
		$Tween.start()
	else:
		self.visible = false
	dialogue_index += 1

func _on_Tween_tween_completed(object, key):
	finished = true
