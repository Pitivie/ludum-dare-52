extends Position2D

var Alien
var station

export var speed = 20
export var timer = 1
export var delay = 5
export var damage = 10
signal alienDestroyed

func _ready():
	Alien = preload("res://Alien.tscn")
	station = get_parent().get_node("Station")
	$spawn.wait_time = timer
	$delay.wait_time = delay
	$delay.start()

func _on_spawn_timeout():
	var alien = Alien.instance()
	alien.position = self.position
	alien.speed = speed
	alien.damage = damage
	alien.target(station.position)
	alien.connect("touched", self, "_on_touched_alien")
	alien.connect("destroyed", self, "_on_destroyed_alien")
	get_parent().add_child(alien)

func _on_touched_alien():
	station.hurt(damage)

func _on_destroyed_alien():
	emit_signal("alienDestroyed")

func _on_delay_timeout():
	$spawn.start()
